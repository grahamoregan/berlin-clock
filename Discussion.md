# Overview

This is a simple application to mimic the software the functionality of a Berlin Clock, see [Mengenlehreuhr](http://en.wikipedia.org/wiki/Mengenlehreuhr) 
for more details.

# Getting Started

The application uses Maven for dependency management and building, to test that the application is passing all tests;
~~~
 $ mvn test
~~~

Maven may need to download some additional binaries but you will eventually be greeted with the following output;

~~~
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 0.833s
[INFO] Finished at: Wed Apr 15 14:10:53 BST 2014
[INFO] Final Memory: 9M/310M
[INFO] ------------------------------------------------------------------------
~~~ 


# Design Overview

The application is a skeumorphic design of a Berlin clock where a clock is composed of a `Mechanism` to maintain and update
the state of the clock, and a `ClockFace` to provide a representation of the time at a given moment. The `BerlinUhrClockFace`
uses an internal array of `Lamp` objects to mimic the original Berlin clock design where each `Lamp` know's its on/off state as well 
as its colour.

The implementation of a Berlin clock is unusual, the time 24:00:00 is considered a valid state where a digital clock would roll over
to 00:00:00. To allow the application to be extended to support the conventional functionality, the `Mechanism` has been abstracted 
into a separate interface with an `AbstractMechanism` providing the base state management and the `BerlinUhrMechanism` providing
the additional validation functionality.

The clock face has also been abstracted into a separate interface to separate the display of the time from the underlying mechanism. If
a new interface, such as a digital display, were to be added, a new implementation of the `ClockFace` interface could be added without
breaking existing clocks.

The `BerlinClock` class itself is very basic, think of it as a case around the mechanism and face. It contains one extra method `setTime()`
which sets the `Mechanism`'s time which, in turn, updates the `ClockFace`. Usually this functionality would be performed by winding a handle
or pressing buttons on the outer case but it seemed unnecessary in a test application.

Finally, an immutable `LocalTime` class has been included which provides a timezone-unaware representation of a point in time. It contains
a static method for parsing a string to an instance, using a regular expression to validate the structure of the string, but the class 
leaves deeper validation to the underlying `Mechanism`.

# Assumptions

## The brief uses the time format "hh::mm:ss" but the test cases use "HH:mm:ss" so the latter has been used throughout the code base.

## The sample output in the brief uses line breaks after each row so the test cases validate that they have been used correctly too.

## The use of an internal array to store the state of the lamps may be contentious because index manipulation can be more difficult to 
understand and debug than truer OO design, but the assumption is that the test is partly OO design, which has been demonstrated elsewhere,
and part coding ability and an internal array requires less code for the same functionality.

## The application was developed using TDD using the JUnit version provided but it does not use BDD. The comments for the test cases include
sample Given/When/Then statements so it would be straightforward to migrate the tests to use a BDD framework such as Cucumber if required
to meet agreed internal standards.

## The only dependencies that have been added are Mockito, for isolating functionality when testing, and Guava for parameter validation. 
Guava *could* easily be removed but has been left in place to provide a truer representation of production code.

## Fixtures have been included in Java code, this may restrict their use outside of a development team and could be replaced with an 
alternative format to facilitate testing in cross-functional teams.



