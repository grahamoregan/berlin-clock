package com.inkglobal.techtest;

/**
 * Provides the base functionality for a clock mechanism. If the rules for validating
 * the times were to be modified then this class could be extended without breaking existing
 * clocks.
 *
 */
public abstract class AbstractMechanism implements Mechanism {

	protected final ClockFace clockFace;

	protected LocalTime time;

	public AbstractMechanism(ClockFace clockFace) {
		super();
		this.clockFace = clockFace;
	}

	protected void updateClockFace() {
		clockFace.refresh(time);
	}

	@Override
	public int getHours() {
		return time.getHours();
	}

	@Override
	public int getMinutes() {
		return time.getMinutes();
	}

	@Override
	public int getSeconds() {
		return time.getSeconds();
	}
}
