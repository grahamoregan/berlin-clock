package com.inkglobal.techtest;

/**
 * Representation of a Lamp for the clock face, encapsulates the
 * on/off state and the colour of the lamp.
 *
 */
public class Lamp {

	private final char colour;

	private boolean on;

	public Lamp(char colour) {
		super();
		this.colour = colour;
	}

	public static final Lamp red() {
		Lamp lamp = new Lamp('R');
		return lamp;
	}

	public static final Lamp yellow() {
		Lamp lamp = new Lamp('Y');
		return lamp;
	}

	public void switchOn() {
		this.on = true;
	}

	public void switchOff() {
		this.on = false;
	}

	public boolean isOn() {
		return on;
	}

	public char getColour() {
		return colour;
	}

	public char toChar() {
		return on ? colour : 'O';
	}

	public String toString() {
		return String.valueOf(toChar());
	}

}
