package com.inkglobal.techtest;

import java.util.Arrays;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;

public class BerlinUhrClockFace implements ClockFace {

	private Lamp[] lamps;

	/**
	 * Instantiate the array of lamps
	 */
	public BerlinUhrClockFace() {
		super();

		lamps = new Lamp[24];

		// first row is yellow
		lamps[0] = Lamp.yellow();

		// next two rows are red
		for (int i = 1; i < 9; i++) {
			lamps[i] = Lamp.red();
		}

		// fourth row is a mix, every 3rd is red
		for (int i = 9; i < 20; i++) {
			lamps[i] = ((i + 1) % 3 == 0) ? Lamp.red() : Lamp.yellow();
		}

		// last row is yellow
		for (int i = 20; i < 24; i++) {
			lamps[i] = Lamp.yellow();
		}

	}

	@Override
	public void refresh(LocalTime time) {

		if (time.getSeconds() % 2 == 0) {
			lamps[0].switchOn();
		} else {
			lamps[0].switchOff();
		}

		updateLamps(time.getHours() / 5, 1, 4);
		updateLamps(time.getHours() % 5, 5, 8);

		updateLamps(time.getMinutes() / 5, 9, 19);
		updateLamps(time.getMinutes() % 5, 20, 23);

	}

	@Override
	public void render() {
		System.out.println(this.toString());
	}

	@VisibleForTesting
	public void updateLamps(int number, int offset, int length) {

		for (int i = offset; i <= length; i++) {

			if (number > 0) {
				lamps[i].switchOn();
			} else {
				lamps[i].switchOff();
			}

			number--;
		}
	}

	public String toString() {

		StringBuilder builder = new StringBuilder();

		builder.append(lamps[0]).append("\n");
		builder.append(toCharArray(1, 4)).append("\n");
		builder.append(toCharArray(5, 8)).append("\n");
		builder.append(toCharArray(9, 19)).append("\n");
		builder.append(toCharArray(20, 23));

		return builder.toString();
	}

	private char[] toCharArray(int start, int end) {

		Preconditions.checkArgument(end >= start, "Array range is invalid, end should be great or equal to the end");

		// need to add one because we are dealing with array indexes
		char[] result = new char[(end - start) + 1];

		for (int i = start; i <= end; i++) {
			result[i - start] = lamps[i].toChar();
		}

		return result;
	}

	@VisibleForTesting
	public Lamp[] viewLamps() {
		return Arrays.copyOf(lamps, lamps.length);
	}
}
