package com.inkglobal.techtest;

import com.google.common.base.Preconditions;

/**
 * A {@link BerlinUhrMechanism} is a simple mechanism with an unusual property, it
 * differentiates between 00:00:00 and 24:00:00 instead of rolling over to 
 * 00:00:00.
 *
 */
public final class BerlinUhrMechanism extends AbstractMechanism {

	public BerlinUhrMechanism(ClockFace clockFace) {
		super(clockFace);
	}

	@Override
	public void setTime(LocalTime time) {

		// simple validations
		Preconditions.checkArgument(time.getHours() >= 0 && time.getHours() <= 24, "Hour value must be between 0 and 24");
		Preconditions.checkArgument(time.getMinutes() >= 0 && time.getMinutes() <= 59, "Minute value must be between 0 and 59");
		Preconditions.checkArgument(time.getSeconds() >= 0 && time.getSeconds() <= 59, "Second value must be between 0 and 59");

		/*
		 * The test cases include 24:00:00 as a valid input even though it isn't a
		 * valid time, after 23:59:59 a clock would should 00:00:00 but for this
		 * test we'll accept 24:00:00 only.
		 */
		if (time.getHours() == 24) {
			Preconditions.checkArgument(time.getMinutes() == 0, "Time has passed midnight, minutes cannot exceed 0");
			Preconditions.checkArgument(time.getSeconds() == 0, "Time has passed midnight, seconds cannot exceed 0");
		}

		this.time = time;
		super.updateClockFace();

	}
}
