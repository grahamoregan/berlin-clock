package com.inkglobal.techtest;

public class InvalidTimeFormatException extends RuntimeException {

	// default as very unlikely to change
	private static final long serialVersionUID = 1L;

	public InvalidTimeFormatException(String message) {
		super(message);
	}

}
