package com.inkglobal.techtest;

import static com.google.common.base.Objects.equal;

import com.google.common.base.Objects;

/**
 * An immutable representation of time of day without knowledge of timezone.
 */
public class LocalTime {

	private int hours;

	private int minutes;

	private int seconds;

	public LocalTime(int hours, int minutes, int seconds) {
		super();
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LocalTime other = (LocalTime) obj;

		return equal(this.hours, other.hours) && equal(this.minutes, other.minutes) && this.seconds == other.seconds;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.hours, this.minutes, this.seconds);
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("hours", hours).add("minutes", minutes).add("seconds", seconds).toString();
	}

	public static LocalTime parse(String time) {

		if (time == null || !time.matches("^(0[0-9]|1[0-9]|2[0-4]):[0-5][0-9]:[0-5][0-9]")) {
			throw new InvalidTimeFormatException("Time expected in format HH:mm:ss");
		}

		String[] parts = time.split(":");

		int hours = Integer.parseInt(parts[0]);
		int minutes = Integer.parseInt(parts[1]);
		int seconds = Integer.parseInt(parts[2]);

		LocalTime lt = new LocalTime(hours, minutes, seconds);

		return lt;
	}

	public int getHours() {
		return hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public int getSeconds() {
		return seconds;
	}

}
