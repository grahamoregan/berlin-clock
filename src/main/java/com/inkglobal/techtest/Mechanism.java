package com.inkglobal.techtest;

public interface Mechanism {

	public void setTime(LocalTime time);

	public int getHours();

	public int getMinutes();

	public int getSeconds();

}
