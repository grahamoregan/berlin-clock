package com.inkglobal.techtest;

public interface ClockFace {

	public void refresh(LocalTime time);

	public void render();

}
