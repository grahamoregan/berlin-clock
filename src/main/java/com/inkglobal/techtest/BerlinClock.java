package com.inkglobal.techtest;

public class BerlinClock {

	private Mechanism mechanism;

	private ClockFace clockFace;

	public BerlinClock(Mechanism mechanism, ClockFace clockFace) {
		super();
		this.mechanism = mechanism;
		this.clockFace = clockFace;
	}

	public void setTime(String time) {
		LocalTime lt = LocalTime.parse(time);
		mechanism.setTime(lt);
	}

	public Mechanism getMechanism() {
		return mechanism;
	}

	public ClockFace getClockFace() {
		return clockFace;
	}

}
