package com.inkglobal.techtest;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 
 * This class contains fixtures for testing the clock and it's components.
 *
 */
public class Fixtures {

	public static Map<String, String> validConversions() {
		Map<String, String> map = Maps.newHashMap();
		map.put("00:00:00", "Y\nOOOO\nOOOO\nOOOOOOOOOOO\nOOOO");
		map.put("13:17:01", "O\nRROO\nRRRO\nYYROOOOOOOO\nYYOO");
		map.put("23:59:59", "O\nRRRR\nRRRO\nYYRYYRYYRYY\nYYYY");
		map.put("24:00:00", "Y\nRRRR\nRRRR\nOOOOOOOOOOO\nOOOO");

		return map;
	}

	/**
	 * All times in this list should be parseable by the clock and 
	 * viewable on the clock face.
	 * @return
	 */
	public static List<String> validTimes() {
		return Lists.newArrayList("00:00:00", "24:00:00", "00:59:00", "00:00:59");
	}

	/**
	 * All times in this list should fail validation, make sure we test for null, 
	 * the rest should be picked up by the regex
	 * @return
	 */
	public static List<String> invalidTimes() {
		return Lists.newArrayList(null, "", "00", "00:00", "00:60:00", "00:00:60");
	}

}
