package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class BerlinClockTest {

	private Mechanism mechanism;

	private BerlinUhrClockFace clockFace;

	private BerlinClock clock;

	@Before
	public void setup() {
		clockFace = new BerlinUhrClockFace();
		mechanism = new BerlinUhrMechanism(clockFace);
		clock = new BerlinClock(mechanism, clockFace);
	}

	/**
	 * Given: A new clock has been initialised
	 * When: The clock is used to set the time and the clock face state is checked
	 * Then: The state of the clock face should match known valid values.
	 * 
	 * This is an end-to-end test that ensures that the whole clock apparatus is
	 * working correctly, i.e. the clock is able to set the mechanism's time and the 
	 * mechanism is updating the clock face as expected.
	 * @throws Exception
	 */
	@Test
	public void testSetTime() throws Exception {
		for (Map.Entry<String, String> entry : Fixtures.validConversions().entrySet()) {
			clock.setTime(entry.getKey());
			String actual = clockFace.toString();
			assertEquals(entry.getValue(), actual);
		}
	}
}
