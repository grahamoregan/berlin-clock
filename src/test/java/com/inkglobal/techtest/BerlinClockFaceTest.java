package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class BerlinClockFaceTest {

	private BerlinUhrClockFace clockFace;

	@Before
	public void setup() throws Exception {
		clockFace = new BerlinUhrClockFace();
	}

	/**
	 * Given: A newly constructed clock face
	 * When: The values of the clock are tested
	 * Then: They should all be in the off state
	 */
	@Test
	public void testOutputAfterConstructor() throws Exception {
		assertEquals("O\nOOOO\nOOOO\nOOOOOOOOOOO\nOOOO", clockFace.toString());

		// make sure that all lamps are off
		for (int i = 0; i < 24; i++) {
			assertFalse(clockFace.viewLamps()[i].isOn());
		}
	}

	/**
	 * Given: A newly constructed clock face
	 * When: The colours of the lamps
	 * Then: They should match the colours in the brief
	 */
	@Test
	public void testLampsAfterConstructor() throws Exception {

		// check the first row has one yellow lamp
		assertEquals('Y', clockFace.viewLamps()[0].getColour());

		// second row should be red lamps
		for (int i : new int[] { 1, 2, 3, 4 }) {
			assertEquals('R', clockFace.viewLamps()[i].getColour());
		}

		// third row should be red lamps
		for (int i : new int[] { 5, 2, 3, 8 }) {
			assertEquals('R', clockFace.viewLamps()[i].getColour());
		}

		// test to make sure that every third lamp in the fourth row is red
		for (int i : new int[] { 11, 14, 17 }) {
			assertEquals('R', clockFace.viewLamps()[i].getColour());
		}

		// test to make sure that every other lamp in the fourth row is yellow
		for (int i : new int[] { 9, 10, 12, 13, 15, 16, 18, 19 }) {
			assertEquals('Y', clockFace.viewLamps()[i].getColour());
		}

		// fifth row should be yellow lamps
		for (int i : new int[] { 20, 21, 22, 23 }) {
			assertEquals('Y', clockFace.viewLamps()[i].getColour());
		}

	}

	/**
	 * Given: A newly constructed clock face
	 * When: The actual state is checked against know valid states
	 * Then: Then actual states should match the known values
	 * @throws Exception
	 */
	@Test
	public void testRefresh() throws Exception {

		for (Map.Entry<String, String> entry : Fixtures.validConversions().entrySet()) {
			clockFace.refresh(LocalTime.parse(entry.getKey()));
			assertEquals(entry.getValue(), clockFace.toString());
		}
	}

	/**
	 * Given: A clock that has been set to 00:00:00
	 * When: The lamps are updated row by row
	 * Then: The on/off state should match the expected output 
	 * 
	 * @throws Exception
	 */
	@Test
	public void testUpdateLamps() throws Exception {

		clockFace.refresh(new LocalTime(0, 0, 0));

		clockFace.updateLamps(2, 1, 4);
		assertEquals("Y\nRROO\nOOOO\nOOOOOOOOOOO\nOOOO", clockFace.toString());

		clockFace.updateLamps(2, 5, 8);
		assertEquals("Y\nRROO\nRROO\nOOOOOOOOOOO\nOOOO", clockFace.toString());

		clockFace.updateLamps(7, 9, 19);
		assertEquals("Y\nRROO\nRROO\nYYRYYRYOOOO\nOOOO", clockFace.toString());

		clockFace.updateLamps(3, 20, 23);
		assertEquals("Y\nRROO\nRROO\nYYRYYRYOOOO\nYYYO", clockFace.toString());

	}

}
