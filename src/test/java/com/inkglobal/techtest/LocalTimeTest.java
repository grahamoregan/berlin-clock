package com.inkglobal.techtest;

import static org.junit.Assert.fail;

import org.junit.Test;

public class LocalTimeTest {

	/**
	 * Given: A list of valid times
	 * When: The times are parsed
	 * Then: The times should be parsed without exception
	 * 
	 * @throws Exception
	 */
	@Test
	public void testValidTimeFormats() throws Exception {

		for (String time : Fixtures.validTimes()) {
			LocalTime.parse(time);
		}
	}

	/**
	 * Given: A list of invalid times
	 * When: The times are parsed
	 * Then: An exception should be thrown for each item
	 *
	 * @throws Exception
	 */
	@Test
	public void testInvalidTimeFormats() throws Exception {
		for (String time : Fixtures.invalidTimes()) {
			try {
				LocalTime.parse(time);
				fail("Time value is invalid, expected an InvalidTimeFormatException for " + time);
			} catch (InvalidTimeFormatException e) {

			}
		}
	}
}
