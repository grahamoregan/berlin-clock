package com.inkglobal.techtest;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * This class isolates the mechanism from the clock face
 * to ensure that it is managing it's state correctly and
 * updating the clock face at the correct time.
 */
public class BerlinUhrMechanismTest {

	@Mock
	private ClockFace clockFace;

	private BerlinUhrMechanism mechanism;

	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		mechanism = new BerlinUhrMechanism(clockFace);
	}

	/**
	 * Given: The mechanism has been initialised 
	 * When: The mechanism's time is set to a valid value
	 * Then: The clock face should be notified of the change
	 * 
	 * @throws Exception
	 */
	@Test
	public void setValidTime() throws Exception {

		LocalTime time = new LocalTime(0, 0, 0);
		mechanism.setTime(time);
		verify(clockFace, times(1)).refresh(time);
	}

	/**
	 * Given: The mechanism has been initialised 
	 * When: The mechanism's time is set to an invalid value
	 * Then: An IllegalArgumentException should be thrown
	 * 
	 * @throws Exception
	 */
	@Test
	public void setInvalidTime() throws Exception {

		try {
			mechanism.setTime(new LocalTime(24, 0, 1));
			fail("Time should not have been accepted");
		} catch (IllegalArgumentException e) {

		}

	}

}
